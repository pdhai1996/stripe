<?php
/**
 * Copyright © Cryozonic Ltd. All rights reserved.
 *
 * @package    Cryozonic_StripePayments
 * @copyright  Copyright © Cryozonic Ltd (http://cryozonic.com)
 * @license    Commercial (See http://cryozonic.com/licenses/stripe.html for details)
 */

namespace Cryozonic\StripePayments\Helper;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\CouldNotSaveException;
use Cryozonic\StripePayments\Model;
use Cryozonic\StripePayments\Model\PaymentMethod;
use Psr\Log\LoggerInterface;
use Magento\Framework\Validator\Exception;
use Cryozonic\StripePayments\Helper\Logger;
use Magenest\Pin\Model\ChargeFactory;

class Api
{
    protected $_rollback;
    protected $_trialAmount;
    protected $_chargeFactory;

    public function __construct(
        Model\Config $config,
        LoggerInterface $logger,
        Generic $helper,
        \Cryozonic\StripePayments\Model\StripeCustomer $customer,
        ChargeFactory $chargeFactory
    ) {
        $this->logger = $logger;
        $this->helper = $helper;
        $this->config = $config;
        $this->_stripeCustomer = $customer;
        $this->_chargeFactory=$chargeFactory;
    }

    public function createToken($params)
    {
        // If the card is already a token, such as from Stripe.js, then don't create a new token
        if (is_string($params['card']) && strpos($params['card'], 'tok_') === 0) return $params['card'];

        try
        {
            $params['card'] = $this->helper->getAvsFields($params['card']);

            $this->validateParams($params);

            $token = \Stripe\Token::create($params);

            if (empty($token['id']) || strpos($token['id'],'tok_') !== 0)
                throw new Exception(__('Sorry, this payment method can not be used at the moment. Try again later.'));

            $this->_stripeCustomer->setCustomerCard($token['card']);

            return $token['id'];
        }
        catch (\Stripe\Error\Card $e)
        {
            $this->helper->dieWithError($e->getMessage(), $e);
        }
        catch (\Stripe\Error $e)
        {
            $this->helper->dieWithError($e->getMessage(), $e);
        }
        catch (\Exception $e)
        {
            if ($this->helper->isAdmin())
                $this->helper->dieWithError($e->getMessage(), $e);
            else
                $this->helper->dieWithError(__("Sorry, we could not complete the checkout process. Please contact us for more help."), $e);
        }
    }

    public function validateParams($params)
    {
        if (is_array($params) && isset($params['card']) && is_array($params['card']) && empty($params['card']['number']))
            throw new \Exception("Unable to use Stripe.js, please see http://store.cryozonic.com/documentation/magento-2-stripe-payments-subscriptions#stripejs");
    }

    public function createCharge($payment, $amount, $capture, $useSavedCard = false)
    {
        try
        {
            $this->_rollback = [];
            $order = $payment->getOrder();

            $switchSubscription = $payment->getAdditionalInformation('switch_subscription');

            if ($switchSubscription)
            {
                $date = \DateTime::createFromFormat("d/m/Y", $switchSubscription['trial_until']);
                $trialEnd = $date->getTimestamp();
                if (!is_numeric($trialEnd) || $trialEnd <= 0)
                    throw new \Exception("Invalid date specified for subscription: ". $switchSubscription['trial_until']);

                // Shipping must be included in the subscription price. See the documentation for an explanation.
                $shippingAmount = $order->getShippingAmount();
                $baseShippingAmount = $order->getBaseShippingAmount();
                $order->setShippingAmount(0);
                $order->setBaseShippingAmount(0);
                $order->setGrandTotal($order->getGrandTotal() - $shippingAmount);
                $order->setBaseGrandTotal($order->getBaseGrandTotal() - $baseShippingAmount);

                $this->createSubscriptions($order, $trialEnd);
                $payment->setIsTransactionClosed(1);
                $payment->setAdditionalInformation('captured', true);
                $sub = $this->_stripeCustomer->getSubscription($switchSubscription['from']);
                $sub->cancel();
                return;
            }
            else if ($useSavedCard) // We are coming here from the admin, capturing an expired authorization
            {
                $token = $this->_stripeCustomer->getDefaultSavedCardFrom($payment);
                $this->customerStripeId = $this->_stripeCustomer->getStripeId();

                if (!$token || !$this->customerStripeId)
                    throw new \Exception('The authorization has expired and the customer has no saved cards to re-create the order.');
            }
            else
            {
                $token = $this->getToken($payment);

                if ($this->helper->hasSubscriptions())
                {
                    // Ensure that a customer exists in Stripe (may be the case with Guest checkouts)
                    if (!$this->_stripeCustomer->getStripeId())
                        $this->_stripeCustomer->createStripeCustomer($order);

                    // We need a saved card for subscriptions
                    if (strpos($token, 'card_') !== 0)
                    {
                        $card = $this->_stripeCustomer->addSavedCard($token);
                        $token = $card->id;
                    }
                }
            }

            if ($this->config->useStoreCurrency())
            {
                $amount = $order->getGrandTotal();
                $currency = $order->getOrderCurrencyCode();
            }
            else
            {
                $amount = $order->getBaseGrandTotal();
                $currency = $order->getBaseCurrencyCode();
            }

            $cents = 100;
            if ($this->helper->isZeroDecimal($currency))
                $cents = 1;

            $metadata = ["Module" => PaymentMethod::module()];
            if ($order->getCustomerIsGuest())
            {
                $customer = $this->helper->getGuestCustomer($order);
                $customerName = $customer->getFirstname() . ' ' . $customer->getLastname();
                $metadata["Guest"] = "Yes";
            }
            else
                $customerName = $order->getCustomerName();

            $params = array(
              "currency" => $currency,
              "source" => $token,
              "description" => "Order #".$order->getRealOrderId().' by '.$customerName,
              "capture" => $capture,
              "metadata" => $metadata
            );

            if ($this->config->isReceiptEmailEnabled() && $this->helper->getCustomerEmail())
                $params["receipt_email"] = $this->helper->getCustomerEmail();

            // If this is a saved card, pass the customer id too
            if (strpos($token,'card_') === 0)
                $params["customer"] = $this->_stripeCustomer->getStripeId();

            $this->validateParams($params);

            $this->_trialAmount = 0;
            $this->createSubscriptions($order);

            // If any subscriptions are configured as trials, do not charge for those right now
            $params["amount"] = round(($amount - $this->_trialAmount) * $cents);

            if ($params["amount"] > 0)
            {
                $charge = \Stripe\Charge::create($params);
                $this->_rollback['charge'] = $charge;
                $payment->setTransactionId($charge->id);
                $payment->setAdditionalInformation('address_line1_check', $charge->source->address_line1_check);
                $payment->setAdditionalInformation('address_zip_check', $charge->source->address_zip_check);
            }

            $payment->setIsTransactionClosed(0);
            $payment->setAdditionalInformation('captured', $capture); // Used in admin for Authorize Only payments
            $payment->setIsFraudDetected(false);
            // $payment->setIsTransactionPending(!$capture);
            
            // todo: return charge object/id here
            $orderId=$order->getRealOrderId();
            $chargeId=$charge->id;
            $data=[
                'order_increment_id'=>$orderId,
                'charge_id'=>$chargeId
            ];
            $chargeRecord=$this->_chargeFactory->create();
            $chargeRecord->setData($data);
            $chargeRecord->save();
        }
        catch (\Stripe\Error\Card $e)
        {
            $this->helper->dieWithError($e->getMessage(), $e);
        }
        catch (\Stripe\Error $e)
        {
            $this->helper->dieWithError($e->getMessage(), $e);
        }
        catch (\Exception $e)
        {
            if ($this->helper->isAdmin())
                $this->helper->dieWithError($e->getMessage(), $e);
            else
                $this->helper->dieWithError(__("Sorry, we could not complete the checkout process. Please contact us for more help."), $e);
        }
    }

    public function createSubscriptions($order, $trialEnd = null)
    {
        $this->_rollback['subscriptions'] = [];

        // Get all the products on the order
        $items = $order->getAllItems();
        foreach ($items as $item)
        {
            $product = $this->helper->loadProductById($item->getProductId());
            if ($product->getCryozonicSubEnabled())
            {
                try
                {
                    $this->createSubscriptionForProduct($product, $order, $item, $trialEnd);
                }
                catch (\Stripe\Error\Card $e)
                {
                    $this->rollback($e->getMessage(), $e);
                }
                catch (\Stripe\Error $e)
                {
                    $this->rollback($e->getMessage(), $e);
                }
                catch (\Exception $e)
                {
                    // We get a \Stripe\Error\InvalidRequest if the customer is purchasing a subscription with a currency
                    // that is different from the currency they used for previous subscription purposes
                    $message = $e->getMessage();
                    if (preg_match('/with currency (\w+)$/', $message, $matches))
                    {
                        $currency = strtoupper($matches[1]);
                        $this->rollback("Your account has been configured to use a different currency. Please complete the purchase in the currency: $currency", $e);
                    }
                    else
                        $this->rollback("Sorry, we could not create the subscription for " . $product->getName() . ". Please contact us for more help.", $e);
                }
            }
        }
    }

    public function createSubscriptionForProduct($product, $order, $item, $trialEnd = null)
    {
        // Get billing interval and billing period
        $interval = $product->getCryozonicSubInterval();
        $intervalCount = $product->getCryozonicSubIntervalCount();

        if (!$interval)
            throw new \Exception(__("An interval period has not been specified for the subscription"));

        if (!$intervalCount)
            $intervalCount = 1;

        // If it is a configurable product, switch to the parent item
        if ($item->getPrice() == 0 && $item->getParentItem() &&
            $item->getParentItem()->getProductType() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE)
        {
            $item = $item->getParentItem();
        }

        // Get the subscription currency and amount
        if ($this->config->useStoreCurrency())
        {
            $amount = $item->getPriceInclTax();
            $currency = $order->getOrderCurrencyCode();
        }
        else
        {
            $amount = $item->getBasePriceInclTax();
            $currency = $order->getBaseCurrencyCode();
        }

        $trialDays = 0;
        if (!$trialEnd)
        {
            $trialDays = $product->getCryozonicSubTrial();
            if (!empty($trialDays) && is_numeric($trialDays) && $trialDays > 0)
            {
                $this->_trialAmount += $amount;
                $trialEnd = strtotime("+$trialDays days");
            }
        }

        $cents = 100;
        if ($this->helper->isZeroDecimal($currency))
            $cents = 1;

        $amount = round($amount * $cents);

        // Generate the plan if it doesn't exist
        $params = [
            'amount' => $amount . $currency,
            'frequency' => $intervalCount . strtoupper($interval) . ($intervalCount > 1 ? 'S' : ''),
            'product' => $product->getId(),
        ];
        $planId = implode('-', $params);
        $plan = $this->getSubscriptionPlan($planId);
        if (!$plan)
        {
            $plan = \Stripe\Plan::create(array(
              "amount" => $amount,
              "interval" => $interval,
              "interval_count" => $intervalCount,
              "name" => $product->getName(),
              "currency" => $currency,
              "id" => $planId
            ));
        }

        // Build the metadata for this subscription - the customer will be able to edit these in the future
        $metadata = [
            "Product ID" => $product->getId(),
            "Customer ID" => $this->_stripeCustomer->getCustomerId(),
            "Order #" => $order->getIncrementId(),
            "Module" => \Cryozonic\StripePayments\Model\PaymentMethod::$moduleName . " v" . \Cryozonic\StripePayments\Model\PaymentMethod::$moduleVersion
        ];
        $shipping = $this->helper->getAddressFrom($order);
        if ($shipping)
        {
            $metadata["Shipping First Name"] = $shipping["firstname"];
            $metadata["Shipping Last Name"] = $shipping["lastname"];
            $metadata["Shipping Company"] = $shipping["company"];
            $metadata["Shipping Street"] = $shipping["street"];
            $metadata["Shipping City"] = $shipping["city"];
            $metadata["Shipping Region"] = $shipping["region"];
            $metadata["Shipping Postcode"] = $shipping["postcode"];
            $metadata["Shipping Country"] = $shipping["country_id"];
            $metadata["Shipping Telephone"] = $shipping["telephone"];
        }

        if ($trialDays > 0)
            $metadata["Trial"] = "$trialDays days";

        // Subscribe the customer to the plan
        $params = [
          "customer" => $this->_stripeCustomer->getStripeId(),
          "plan" => $planId,
          "trial_end" => ($trialEnd ? $trialEnd : strtotime("+$intervalCount $interval")),
          "quantity" => $item->getQtyOrdered(),
          "metadata" => $metadata
        ];
        $subscription = \Stripe\Subscription::create($params);
        $this->_rollback['subscriptions'][] = $subscription;

        $order->addStatusHistoryComment("Subscribed to {$this->helper->formatSubscriptionName($subscription)} ({$subscription->id})");
    }

    public function getSubscriptionPlan($planId)
    {
        try
        {
            return \Stripe\Plan::retrieve($planId);
        }
        catch (\Exception $e) {}

        return null;
    }

    public function rollback($msg = null, $e = null)
    {
        if (!$msg)
            $msg = "Sorry, we could not create the subscription. Please contact us for more help.";

        // Refund the order charge
        if (isset($this->_rollback['charge']))
            $this->_rollback['charge']->refund();

        // Unsubscribe the customer from all new subscriptions
        if (isset($this->_rollback['subscriptions']))
        {
            foreach ($this->_rollback['subscriptions'] as $subscription)
                $subscription->cancel();
        }

        // Stop the Magento checkout
        $this->helper->dieWithError($msg, $e);
    }

    public function getToken($info)
    {
        $token = $info->getAdditionalInformation('token');

        // Is this a saved card?
        if (strpos($token,'card_') === 0)
            return $token;

        // Are we coming from the back office?
        if (strstr($token,'tok_') === false)
        {
            $params = $this->getInfoInstanceCard($info);
            $token = $this->createToken($params);
        }

        return $token;
    }

    public function getInfoInstanceCard($info)
    {
        return array(
            "card" => array(
                "name" => $info->getCcOwner(),
                "number" => $info->getCcNumber(),
                "cvc" => $info->getCcCid(),
                "exp_month" => $info->getCcExpMonth(),
                "exp_year" => $info->getCcExpYear()
            )
        );
    }
}