<?php
/**
 * Copyright © Cryozonic Ltd. All rights reserved.
 *
 * @package    Cryozonic_StripePayments
 * @copyright  Copyright © Cryozonic Ltd (http://cryozonic.com)
 * @license    Commercial (See http://cryozonic.com/licenses/stripe.html for details)
 */

namespace Cryozonic\StripePayments\Block;

use Cryozonic\StripePayments\Helper\Logger;

class Form extends \Magento\Payment\Block\Form\Cc
{
    protected $_template = 'form/cryozonic_stripe.phtml';

    public $config;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Payment\Model\Config $paymentConfig,
        \Cryozonic\StripePayments\Model\Config $config,
        \Cryozonic\StripePayments\Helper\Generic $helper,
        \Magento\Backend\Model\Session\Quote $sessionQuote,
        \Cryozonic\StripePayments\Model\StripeCustomer $stripeCustomer,
        array $data = []
    ) {
        parent::__construct($context, $paymentConfig, $data);
        $this->config = $config;
        $this->helper = $helper;
        $this->sessionQuote = $sessionQuote;
        $this->stripeCustomer = $stripeCustomer;
    }

    public function isSingleSubscriptionPurchase()
    {
        $isSubscription = false;
        $quote = $this->sessionQuote->getQuote();
        $itemsCount = $quote->getItemsCount();

        if ($itemsCount != 1) return false;

        foreach ($quote->getAllItems() as $item) {
            $product = $item->getProduct();
            $product = $product->load($product->getId());
            $isSubscription = $product->getCryozonicSubEnabled();
        }

        return $isSubscription;
    }

    public function formatSubscriptionName($sub)
    {
        return $this->helper->formatSubscriptionName($sub) . " - ends " . $this->formatSubscriptionPeriodEnd($sub);
    }

    public function formatSubscriptionPeriodEnd($sub)
    {
        return date("d/m/Y", $sub->current_period_end);
    }

    public function getCustomerCards()
    {
        return $this->stripeCustomer->listCards();
    }

    public function getCustomerSubscriptions()
    {
        return $this->stripeCustomer->getSubscriptions();
    }
}
